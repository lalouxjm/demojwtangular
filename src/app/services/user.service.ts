import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _url = "https://localhost:7064/api/user"

  constructor(private readonly _client: HttpClient) { }

  token!: string;

  login(email: string, passwd: string){
    this._client.post(this._url, { email, passwd }, { responseType: "text" } ).subscribe({
      next: (token) => {
        console.log(token);
        this.token = token;
      },
      error: (error) => {
        console.log(error);
        console.log(JSON.parse(error.error).errors);
      }
  })
}

  getAll(){
    // let myHeader: HttpHeaders = new HttpHeaders({"Authorization" : "bearer " + this.token});
    // myHeader.append("Authorization", "bearer " + this.token);
    this._client.get<User[]>(this._url/*, { headers: myHeader }*/).subscribe({
      next: (list: User[]) => console.log(list),
      error: (error) => {
        console.log(error);
        console.log(JSON.parse(error.error).errors);
      }
    });
  }

  switchRole(id: number){
    // let myHeader: HttpHeaders = new HttpHeaders({"Authorization" : "bearer " + this.token});
    // myHeader.append("Authorization", "bearer " + this.token);
    this._client.patch(this._url + "/" + id, null/*, { headers: myHeader }*/).subscribe({
      next: () => console.log("Role updated"),
      error: (error) => {
        console.log(error);
        console.log(JSON.parse(error.error).errors);
      }
    });
  }

}

export interface User {
   id: number
   email: string
   nickname: string 
   isAdmin: boolean 
}