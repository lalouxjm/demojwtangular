import { Component } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent {

  constructor(private readonly _service: UserService){}

  login(){
    this._service.login("admin@test.com", "test1234");
  }

  getAll(){
    this._service.getAll();
  }

  switchRole(){
    this._service.switchRole(2);
  }
}
